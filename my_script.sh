# Assuming you have already downloaded the Docker image artifact as `myflask_latest.tar.gz`
tar xvf myflask_latest.tar.gz

# Load the Docker image from the artifact
sudo docker load -i myflask_latest.tar

# Tag the Docker image with the desired repository name and tag
sudo docker tag myflask:latest $DOCKER_REGISTRY_USER/myflask:latest

# Log in to Docker registry
echo "$DOCKER_REGISTRY_PASSWORD" | sudo docker login -u "$DOCKER_REGISTRY_USER" --password-stdin

# Push the Docker image to Docker registry
sudo docker push $DOCKER_REGISTRY_USER/myflask:latest
