# test_blob_storage.py

import os
import pytest
from azure.storage.blob import BlobServiceClient

# @pytest.fixture(scope="session")
# def blob_service_client():
#     connection_string = os.getenv("CONNECTION_STRING")
#     blob_service_client = BlobServiceClient.from_connection_string(connection_string)
#     yield blob_service_client

# @pytest.fixture(scope="session")
# def test_file_path():
#     return os.path.abspath("test_file.txt")

# def test_upload_file_to_blob_storage(blob_service_client, test_file_path):
#     container_name = "test-container"
#     blob_name = "test-blob"

#     try:
#         # Upload the file to Azure Blob Storage
#         with open(test_file_path, "rb") as data:
#             blob_service_client.get_blob_client(container=container_name, blob=blob_name).upload_blob(data)

#         # Check if the blob exists in the container
#         blob_client = blob_service_client.get_blob_client(container=container_name, blob=blob_name)
#         assert blob_client.exists()

#         return True  # Return True if the upload was successful
#     except Exception as e:
#         print(f"Upload failed: {str(e)}")
#         return False  # Return False if the upload failed
def always_true():
    return True

# Test the function
if __name__ == "__main__":
    print(always_true())
